package com.example.angelserafim.createphoto;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends Activity {

    private final int CAMERA_RESULT = 0;
    private String pathToPhoto = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

/*
        DB myDB = new DB(this, "photoDB.db", null, 1);
        SQLiteDatabase SDB = myDB.getReadableDatabase();
        SDB.execSQL("DELETE FROM photoTable;");
*/
    }

    public void inGallery(View view) {
        Button inGallery = (Button) findViewById(R.id.button1);
        DB myDB = new DB(this, "photoDB.db", null, 1);
        final SQLiteDatabase SDB = myDB.getReadableDatabase();

        inGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(SDB.isOpen()) {
                    Cursor cursor = SDB.query("photoTable", new String[] {"photoColumn"}, null, null, null, null, null);
                    if(cursor.getCount() == 0)
                        Toast.makeText(MainActivity.this, "Галерея пуста", Toast.LENGTH_SHORT).show();
                    else
                        startActivity(new Intent(MainActivity.this, Main2Activity.class));
                }
            }
        });
    }

    public void makePhoto(View view) {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        //startActivity(new Intent(MainActivity.this, MyCamera.class));

        pathToPhoto = new SimpleDateFormat("dd_MM_yyyy-HH_mm_ss").format(new Date());
        pathToPhoto = "/storage/emulated/0/Pictures/" + pathToPhoto + ".jpg";

        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(pathToPhoto)));
        startActivityForResult(cameraIntent, CAMERA_RESULT);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CAMERA_RESULT) {
            if (resultCode == RESULT_OK) {

                DB myDB = new DB(this, "photoDB.db", null, 1);
                SQLiteDatabase SDB = myDB.getReadableDatabase();
                if(SDB.isOpen())
                    SDB.execSQL("INSERT INTO photoTable (photoColumn) VALUES ('" + pathToPhoto + "');");
                else
                    Toast.makeText(MainActivity.this, "БД не открыта", Toast.LENGTH_SHORT).show();

                Toast.makeText(getApplicationContext(), "Фотография сделана", Toast.LENGTH_LONG).show();
            }
            if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(), "Фотография не сделана", Toast.LENGTH_LONG).show();
            }
        }
    }

}
